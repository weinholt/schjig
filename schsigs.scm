;; -*- coding: utf-8; mode: scheme -*-

;; Copyright (C) 2013 Göran Weinholt <goran@weinholt.se>

;; Permission is hereby granted, free of charge, to any person obtaining a
;; copy of this software and associated documentation files (the "Software"),
;; to deal in the Software without restriction, including without limitation
;; the rights to use, copy, modify, merge, publish, distribute, sublicense,
;; and/or sell copies of the Software, and to permit persons to whom the
;; Software is furnished to do so, subject to the following conditions:

;; The above copyright notice and this permission notice shall be included in
;; all copies or substantial portions of the Software.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;; DEALINGS IN THE SOFTWARE.
#!r6rs

;; Automatically infer signatures for Scheme procedures.

;; This program goes through most standard procedures and outputs a
;; type signature for each of them. The types are less specific than
;; those used in the R6RS document. Interesting facts can be found by
;; examining the output, e.g. by comparing it with the output from
;; another R6RS implementation.

(import (rnrs)
        (rnrs eval)
        (only (srfi :1 lists)
              append-map filter-map
              delete-duplicates iota
              last span))

(define *maxargs* 5)

(define (print . x) (for-each display x) (newline))

;; The trace might be interesting if schinf manages to crash, but it's
;; really really verbose.
(define-syntax trace
  (syntax-rules ()
    ;; ((_ . args) (begin (for-each write (list . args)) (newline)))
    ((_ . args) (values))))

(define-syntax debug
  (syntax-rules ()
    ;; ((_ . args) (begin (for-each display (list . args)) (newline)))
    ((_ . args) (values))))

(define procedure-names
  '(number?
    complex? real? rational? integer? real-valued? rational-valued?
    integer-valued? exact? inexact? inexact exact = < > <= >= zero?
    positive? negative? odd? even? finite? infinite? nan? max min +
    * - / abs div-and-mod div mod div0-and-mod0 div0 mod0 gcd lcm
    numerator denominator floor ceiling truncate round rationalize
    exp log sin cos tan asin acos atan sqrt exact-integer-sqrt expt
    make-rectangular make-polar real-part imag-part magnitude angle

    bitwise-not bitwise-and bitwise-ior bitwise-xor bitwise-if
    bitwise-bit-count bitwise-length bitwise-first-bit-set
    bitwise-bit-set? bitwise-copy-bit bitwise-bit-field
    bitwise-copy-bit-field bitwise-arithmetic-shift
    bitwise-arithmetic-shift-left bitwise-arithmetic-shift-right
    bitwise-rotate-bit-field bitwise-reverse-bit-field

    inexact exact->inexact exact inexact->exact quotient remainder modulo

    number->string string->number

    not boolean? boolean=?

    native-endianness bytevector? make-bytevector bytevector-length
    bytevector=? bytevector-fill! bytevector-copy! bytevector-copy

    bytevector-u8-ref bytevector-s8-ref
    bytevector-u8-set! bytevector-s8-set!
    bytevector->u8-list u8-list->bytevector
    bytevector-uint-ref bytevector-sint-ref
    bytevector-uint-set! bytevector-sint-set!
    bytevector->uint-list bytevector->sint-list
    uint-list->bytevector sint-list->bytevector
    bytevector-u16-ref bytevector-s16-ref
    bytevector-u16-native-ref bytevector-s16-native-ref
    bytevector-u16-set! bytevector-s16-set!
    bytevector-u16-native-set! bytevector-s16-native-set!
    bytevector-u32-ref bytevector-s32-ref
    bytevector-u32-native-ref bytevector-s32-native-ref
    bytevector-u32-set! bytevector-s32-set!
    bytevector-u32-native-set! bytevector-s32-native-set!
    bytevector-u64-ref bytevector-s64-ref
    bytevector-u64-native-ref bytevector-s64-native-ref
    bytevector-u64-set! bytevector-s64-set!
    bytevector-u64-native-set! bytevector-s64-native-set!
    bytevector-ieee-single-native-ref bytevector-ieee-single-ref
    bytevector-ieee-double-native-ref bytevector-ieee-double-ref
    bytevector-ieee-single-native-set! bytevector-ieee-single-set!
    bytevector-ieee-double-native-set! bytevector-ieee-double-set!
    string->utf8 string->utf16 string->utf32
    utf8->string utf16->string utf32->string

    char? char->integer integer->char
    char=? char<? char>? char<=? char>=?
    char-upcase char-downcase char-titlecase char-foldcase
    char-ci=? char-ci<? char-ci>? char-ci<=? char-ci>=?
    char-alphabetic? char-numeric? char-whitespace?
    char-upper-case? char-lower-case? char-title-case?
    char-general-category

    condition? message-condition? warning? serious-condition? error?
    violation? assertion-violation? irritants-condition?
    who-condition? non-continuable-violation?
    implementation-restriction-violation? lexical-violation?
    syntax-violation? undefined-violation? i/o-error? i/o-read-error?
    i/o-write-error? i/o-invalid-position-error? i/o-filename-error?
    i/o-file-protection-error? i/o-file-is-read-only-error?
    i/o-file-already-exists-error? i/o-file-does-not-exist-error?
    i/o-port-error? i/o-decoding-error? i/o-encoding-error?
    no-infinities-violation? no-nans-violation?

    procedure? command-line

    eq? eqv? equal?

    fixnum? fixnum-width least-fixnum greatest-fixnum
    fx=? fx>? fx<? fx>=? fx<=? fxzero? fxpositive? fxnegative?
    fxodd? fxeven? fxmax fxmin fx+ fx- fx* fxdiv-and-mod
    fxdiv fxmod fxdiv0-and-mod0 fxdiv0 fxmod0
    fx+/carry fx-/carry fx*/carry fxnot fxand fxior fxxor
    fxif fxbit-count fxlength fxfirst-bit-set fxbit-set?
    fxcopy-bit fxbit-field fxcopy-bit-field fxarithmetic-shift
    fxarithmetic-shift-left fxarithmetic-shift-right
    fxrotate-bit-field fxreverse-bit-field

    flonum? real->flonum fl=? fl<? fl>? fl<=? fl>=? flinteger?
    flzero? flpositive? flnegative? flodd? fleven? flfinite?
    flinfinite? flnan? flmax flmin fl+ fl* fl- fl/ flabs
    fldiv-and-mod fldiv flmod fldiv0-and-mod0 fldiv0 flmod0
    flnumerator fldenominator flfloor flceiling fltruncate
    flround flexp fllog fllog flsin flcos fltan flasin flacos
    flatan flatan flsqrt flexpt fixnum->flonum

    equal-hash string-hash string-ci-hash symbol-hash

    buffer-mode? eof-object eof-object?
    port? input-port? output-port?

    pair? cons car caar caaar caaaar cdaaar cdaar cadaar cddaar
    cdar cadar caadar cdadar cddar caddar cdddar cdr cadr caadr
    caaadr cdaadr cdadr cadadr cddadr cddr caddr caaddr cdaddr
    cdddr cadddr cddddr null? list? list length append reverse
    list-tail list-ref map for-each find for-all exists filter
    partition fold-left fold-right remp remove remv remq memp
    member memv memq assp assoc assv assq cons*

    record-type-descriptor? record?

    list-sort vector-sort vector-sort!

    string? make-string string string-length string-ref string=?
    string<? string>? string<=? string>=? substring string-append
    string->list list->string string-for-each string-copy
    ;; string-fill! is disabled because it somehow segfaults Ikarus.
    #;string-fill! string-set! string-ci=? string-ci<? string-ci>?
    string-ci<=? string-ci>=? string-upcase string-downcase
    string-foldcase string-titlecase string-normalize-nfd
    string-normalize-nfkd string-normalize-nfc string-normalize-nfkc

    symbol? symbol->string symbol=? string->symbol

    vector? make-vector vector vector-length vector-ref vector-set!
    vector->list list->vector vector-fill! vector-map vector-for-each

    identifier?

    set-car! set-cdr!))

(define env
  (environment '(rnrs)
               '(rnrs mutable-strings)
               '(rnrs mutable-pairs)
               '(rnrs r5rs)))

;; It should be possible to invoke most standard procedures using a
;; combination of the cdrs of this list.
(define instance*
  `((number . 8)
    (number . 1.0)
    (number . 0)
    (number . 10)
    (char . #\0)
    (boolean . #f)
    (pair . ,(let subtree ((level 0))
               ;; XXX: For Racket you will want to reduce the number
               ;; of levels here, because apparently this tree gets
               ;; traversed when there's an arity mismatch.
               (if (= level 5)
                   'a
                   (let ((i (+ level 1)))
                     (map subtree (list i i i i
                                        i i i i
                                        i))))))
    (list . ())
    (symbol . scheme)
    (string . ,(make-string 16))
    (vector . ,(make-vector 16))
    (bytevector . ,(make-bytevector 16))
    (endianness . little)
    (procedure . ,(lambda x #f))))

(define instance-type*
  (delete-duplicates (map car instance*)))

(define (make-instance type)
  (cond ((assq type instance*) => cdr)
        (else
         (case type
           ((obj)
            ;; An obj is supposed to be an arbitrary type, so make a
            ;; new type.
            (let ()
              (define-record-type foo (nongenerative foo))
              (make-foo)))
           (else
            (error 'make-instance
                   "Can't make an instance of type" type))))))

;; Generate all length n combinations of the list entries.
(define (combinations x* n)
  (case n
    ((0) '(()))
    ((1) (map list x*))
    (else
     (append-map (lambda (x)
                   (map (lambda (y) (cons x y))
                        (combinations x* (- n 1))))
                 x*))))

(define instance-combinations
  (list->vector
   (map (lambda (n) (combinations instance* n))
        (iota (+ *maxargs* 1)))))

;; A procedure has been applied to a few arguments. For one of the
;; arguments the procedure accepted the types given to this procedure.
;; Now the job is to find the most general type that works for that
;; argument. They arrive in the same order as in instance* and there is
;; always at least one type.
(define (least-specific-type x*)
  (define (symbol<? x y)
    (string<? (symbol->string x)
              (symbol->string y)))
  (let ((x* (delete-duplicates x*)))
    (cond
      ((null? (cdr x*))
       (car x*))
      ((equal? x* '(pair list))
       'pair)
      ((equal? x* '(list pair))
       ;; Can happen for (map x '())
       'list)
      ((equal? x* '(symbol endianness))
       'symbol)
      ((equal? x* instance-type*)
       ;; Anything goes, apparently.
       'obj)
      ((equal? (list-sort symbol<? x*)
               (list-sort symbol<? instance-type*))
       ;; Anything goes, except sometimes it doesn't.
       'obj)
      (else
       `(or ,@x*)))))

;; Determine the type requirements for the arguments to proc.
(define (determine-types proc arg-num)
  (let ((type* (filter-map
                (lambda (arg*)
                  (guard (exn (else (trace (condition-message exn)) #f))
                    (let ((obj* (map cdr arg*)))
                      (trace "  " obj*)
                      (apply proc obj*)
                      (trace "OK!")
                      (map car arg*))))
                (vector-ref instance-combinations arg-num))))
    (if (null? type*)
        #f                              ;No application worked
        (let lp ((type* type*))         ;Workaround for Vicare's apply
          (if (for-all null? type*)
              '()
              (cons (least-specific-type (map car type*))
                    (lp (map cdr type*)))))
        #;(apply map least-specific-type type*))))

;; Find how many arguments the procedure accepts and their types. If
;; the procedure can take at least four arguments, then it doesn't
;; check if it can take any more. This is to make it faster.
(define (procedure-signatures proc)
  (let lp ((i 0)
           (ret* '()))
    (cond ((or (> i *maxargs*)
               (and (> i 4)
                    (not (null? ret*))))
           (simplify-signatures proc ret*))
          ((determine-types proc i)
           => (lambda (type*)
                (lp (+ i 1) (cons type* ret*))))
          (else
           (lp (+ i 1) ret*)))))

;; There's a list of signatures, one per arity. This is not very
;; informative for n-ary functions. This is sig* for vector-map:
;; ((procedure vector vector vector)
;;  (procedure vector vector)
;;  (procedure vector))
;; This would turn into (procedure vector ...).
(define (simplify-signatures proc sig*)
  (if (< (length sig*) 2)
      (reverse sig*)
      (let* ((last-type (last (car sig*)))
             (rev-longest (reverse (car sig*))))
        (let-values (((last* other*)
                      (span (lambda (x) (eq? x last-type))
                            rev-longest)))
          (let ((other* (reverse other*)))
            (guard (_ ((null? other*)
                       ;; The procedure did not accept the new
                       ;; arguments, and all the other arguments are
                       ;; the same as the last type.
                       (reverse sig*))
                      (else
                       ;; Maybe the last argument is special (as for
                       ;; append).
                       (let ((sig^* (filter-map
                                     (lambda (sig)
                                       (and (pair? sig)
                                            (eq? last-type (last sig))
                                            (reverse (cdr (reverse sig)))))
                                     sig*)))
                         (let ((sig^^* (simplify-signatures proc sig^*)))
                           (cond
                             ((memp (lambda (sig) (memq '... sig)) sig^^*)
                              ;; Yes, it is n-ary with a special
                              ;; argument at the end.
                              `(,@(filter null? sig*)
                                ,@(map (lambda (sig)
                                         `(,@sig ,last-type))
                                       sig^^*)))
                             (else
                              ;; Nope. No ... in this sig*.
                              (reverse sig*)))))))
              ;; See if it really is n-ary by doubling the number of
              ;; arguments that have the same type as the last argument.
              (let ((arg* (map make-instance (append (car sig*) last*))))
                (guard (_ (else
                           ;; If the first attempt doesn't work, then
                           ;; this might be a flonum procedure that
                           ;; was passed fixnums.
                           (apply proc (map (lambda (x)
                                              (if (number? x)
                                                  (fixnum->flonum x)
                                                  x))
                                            arg*))))
                  (apply proc arg*)))
              ;; First the shortest signature that doesn't end with
              ;; last-type (plus some consistency checks).
              (let lp ((sig^* sig*)
                       (last* last*))
                ;; (display (list 'sig^* sig^* 'last* last* 'other* other*)) (newline)
                (cond ((null? sig^*) (reverse sig*))
                      ((and (equal? (car sig^*) `(,@other* ,last-type))
                            (equal? last* `(,last-type)))
                       (cond
                         ((and (null? other*)
                               (equal? (cdr sig^*) '(())))
                          ;; The procedure only takes optional
                          ;; arguments.
                          `((,@other* ,last-type ...)))
                         (else
                          ;; The last non-optional argument has a
                          ;; type different from last-type.
                          (cond ((equal? (cdr sig^*) `(,other*))
                                 ;; But having an extra signature
                                 ;; would be redundant, since the
                                 ;; other types are already part of
                                 ;; this signature.
                                 `((,@other* ,last-type ...)))
                                (else
                                 (reverse
                                  (cons `(,@other* ,last-type ...)
                                        (cdr sig^*))))))))
                      ((and (null? (cdr sig^*))
                            (equal? (car sig^*) last*))
                       ;; The last non-optional argument has the same
                       ;; type as last-type.
                       (reverse
                        (cons `(,@(car sig^*) ,last-type ...)
                              (cdr sig^*))))
                      ((equal? (car sig^*)
                               (append other* last*))
                       ;; Bite off one last-type argument.
                       (lp (cdr sig^*) (cdr last*)))
                      (else
                       (reverse sig*))))))))))

(define (analyze-procedure name)
  (debug ";; Finding signatures for " name)
  (flush-output-port (current-output-port))
  (let ((sig* (map (lambda (type*) (cons name type*))
                   (procedure-signatures (eval name env)))))
    (when (null? sig*)
      (print ";; No signature for " name))
    (for-each print sig*)
    (flush-output-port (current-output-port))))

(for-each analyze-procedure procedure-names)
