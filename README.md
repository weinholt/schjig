Automatic test generation for Scheme
====================================

Finding myself too lazy to write test cases for Scheme, I decided to try and make the computer write them for me. In the literature one approach to this problem is fuzzing, which can be done in a variety of ways. A very simple way to do fuzzing is to call procedures with random arguments. There are many types of bugs that will not be found with this method, but it's a good start.

The approach taken by schjig is to generate procedure calls where the arguments are in the expected domain (or at least close to). The procedure calls are eval'd and printed along with the computed result. The output is a Scheme program that can be run in another Scheme implementation to verify that it computes the same results. Some false positives are to be expected, especially when it comes to inexact numbers.

The current version only handles non-compound data types and it targets R6RS Scheme. Before using schjig, you might like to change the settings at the top of the script.

Bugs found with these tools
---------------------------

GNU Guile 2.0.9 (amd64):

* 14863 bitwise-bit-count is wrong for negative arguments

  (bitwise-bit-count #b-101)
  => 1

* 14864 bitwise-arithmetic-shift-right is wrong for large shift amounts

  (bitwise-arithmetic-shift-right -2 (+ (greatest-fixnum) 1))
  => -2

* 14865 flmax and flmin do not handle +nan.0 correctly

  (flmax +inf.0 +nan.0)
  => +inf.0

* 14866 flonum? gives #t for complex number objects

  (flonum? 1.0+1.0i)
  => #t

* 14867 Infinity less than commonly held

  (= 1/2 +inf.0)
  => #t

* 14868 flfinite? +nan.0 returns #t

  (flfinite? +nan.0)
  => #t

* 14869 fl+ and fl* should also accept 0 arguments

  (fl*)
  An exception is raised.

* 14870 gcd and lcm do not work on inexact integers

  (lcm 32.0 -36)
  An exception is raised.

* 14871 flonum procedures returning non-flonum objects

  (fllog -1.0)
  => 0.0+3.141592653589793i

* 14879 fixnum-width, greatest-fixnum and least-fixnum disagree

  (greatest-fixnum) differs from (- (expt 2 (- (fixnum-width) 1)) 1)

* 14905 rationalize not returning the simplest rational

  (rationalize #e-0.67 1/4) returns -2/3 instead of -1/2

* 14916 Fixnum procedures can be made to return non-fixnums

  (fxdiv (least-fixnum) -1) returns (+ (greatest-fixnum) 1).

* 14917 Missing range check in fxcopy-bit can give SIGABRT

  (fxcopy-bit 0 1000000000000 0)
  The process is killed by SIGABRT.

* 15100 Division by zero in bytevector->uint-list

  (bytevector->uint-list #vu8(0) 'x 0)
  The process is killed by SIGFPE.

* 15123 Missing R6RS io port exports

  Several bindings weren't exported by (rnrs). Found with libs.scm.

* 15140 number->string doesn't accept the optional precision argument

  (number->string 1.1 10 53)
  An exception is raised. Found with schsigs.scm.

Petite Chez Scheme 8.4 (a6le):

* Undetected overflow in fxarithmetic-shift-left.

  (fxarithmetic-shift-left #xB00000000000000 2)
  => #xC00000000000000

  (fxarithmetic-shift-left #x-500000000000000 3)
  => #x-800000000000000

Racket 5.3.5:

* all/13922: fl+ and fl* returning exact integers

  (fl+) and (fl*) returned 0 and 1.

* all/13923: bitwise-arithmetic-shift-right wrong for large shift amounts

  (bitwise-arithmetic-shift-right 42 (+ 1 (greatest-fixnum)))
  => 42

* all/13925: mod returning a negative number

  (mod 2.9e+18 -1/1000000000000000)
  => -512.0

Vicare Scheme 0.3d1, 64-bit:

* Issue #50: bitwise-arithmetic-shift-right doesn't handle bignum shift amounts

  (bitwise-arithmetic-shift-right -1 (+ (greatest-fixnum) 1))
  An exception is raised.

* Issue #51: Negative result from gcd

  (gcd -1)
  => -1

* Issue #52: (bitwise-ior) and (bitwise-xor) should both be zero

  (bitwise-ior)
  => -1

  (bitwise-xor)
  => -1

* Issue #53: Segfault when port? is called with no arguments

  (port?)
  The process is killed by SIGSEGV.

* Issue #54: Segfault when fl- is called with a fixnum

  (fl- 1)
  The process is killed by SIGSEGV.

IronScheme TFS:101169 (.NET 4.0 64-bit)

* 20125: div and mod are wrong for some arguments

  (This was accidentally reported with an incorrect example.)

  (fxdiv-and-mod -2147483648 -1825363635)
  => 0 -2147483648

* 20126: NaNs are not finite

  (finite? +nan.0)
   => #t

* 20127: NaN should propagate through max and min

  (max +inf.0 +nan.0)
  => +inf.0

* 20128: Rounding is wrong for exact rationals

  (round 1/2)
  => 1

* 20129: The exact-integer-sqrt returns strange number objects

  (let-values (((x y) (exact-integer-sqrt 4294967312)))
    (list (= y 16) (eqv? y 16)))
  => (#t #f)

* 20130: Stack overflow in bitwise-bit-count

  (bitwise-bit-count 0)
  Process is terminated due to StackOverflowException.

* 20131: Too strict argument check in bitwise-rotate-bit-field

  (bitwise-rotate-bit-field 1 1 1 1)
  An &assertion is raised.

* 20132: Too strict argument check in fxbit-set?

  (fxbit-set? -1 100)
  An &assertion is raised.

* 20133: Insufficient argument check in fxarithmetic-shift-right

  (fxarithmetic-shift-right 42 (fixnum-width))
  => 42

* 20134: Wrong result for (expt -2 -inf.0)

  (expt -2 -inf.0)
   => +nan.0+nan.0i

* 20135: Silently wrong result from integer->char

  (= (char->integer (integer->char #x10000)) #x10000)
  => #f

* 20136: The fl<=? and fl>=? procedures are inconsistent with fl<? and fl>? for NaN arguments.

  (fl<=? +inf.0 +nan.0)
  => #t

  (fl<? +inf.0 +nan.0)
  => #f

Sagittarius Scheme 0.9.8

* Issue #286: Sagittarius dumps on Göran Weinholt's schjig

  bytevector->u8-list was missing typechecking; it mustn’t accept a list and must instead raise an &assertion.

